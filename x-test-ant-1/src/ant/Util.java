package ant;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.EnumSet;
import java.util.Properties;

public class Util {
	
	//-------
	// Constantes
	//-------
	
	public static final String DIR_CONFIG = "config";
	public static final String DIR_SERVERS = DIR_CONFIG + "/servers";
	public static final String PROPS_LOGIING = DIR_CONFIG + "/logging.properties";
	public static final String PROPS_GLOBAL = DIR_CONFIG + "/global.properties";
	public static final String JAR_ANT_LAUNCHµER = "ant-launcher.jar";
	
	
	
	//-------
	// Méthodes utilitaires
	//-------
	
	public static Path getPathServers() throws URISyntaxException  {
		var urlServers = Thread.currentThread().getContextClassLoader().getResource( DIR_SERVERS );
		return Paths.get( urlServers.toURI() );
	}
	
	public static Path getPath( String stringPath ) throws URISyntaxException  {
		var url = Thread.currentThread().getContextClassLoader().getResource( stringPath );
		return Paths.get( url.toURI() );
	}
	
	public static InputStream getInputStream( String stringPath ) throws IOException {
		var url = Thread.currentThread().getContextClassLoader().getResource( stringPath );
		if ( url == null ) {
			throw new FileNotFoundException( stringPath + " not found in classpath."  );
		}
		return url.openStream();
	}

	public static Properties getProperties( String stringPath ) throws IOException {
		try ( var in = getInputStream(stringPath) ) {
			var props = new Properties();
			props.load(in);
			return props;
		}
	}
	

	public static void deleteDir( Path path) throws IOException {

		if ( Files.notExists( path ) ) {
			return;
		}
		
		Files.walkFileTree(path, 
			new SimpleFileVisitor<Path>() {
			
			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				Files.delete(file);
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				Files.delete(dir);
				return FileVisitResult.CONTINUE;
			}
		});
	}

	public static void copyDirectory( final Path pathSource, final Path pathTarget) throws IOException {
		
		Files.walkFileTree(pathSource, EnumSet.of(FileVisitOption.FOLLOW_LINKS), Integer.MAX_VALUE,
				new SimpleFileVisitor<Path>() {

			    @Override
			    public FileVisitResult preVisitDirectory(final Path dir,
			    final BasicFileAttributes attrs) throws IOException {
			        Files.createDirectories(pathTarget.resolve(pathSource
			                    .relativize(dir)));
			        return FileVisitResult.CONTINUE;
			    }

			    @Override
			    public FileVisitResult visitFile(final Path file,
			    final BasicFileAttributes attrs) throws IOException {
			    Files.copy(file,
			        pathTarget.resolve(pathSource.relativize(file)), StandardCopyOption.REPLACE_EXISTING );
			    return FileVisitResult.CONTINUE;
			    }
			} );
	}
}
