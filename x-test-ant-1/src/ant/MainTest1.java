package ant;

import java.awt.GraphicsEnvironment;
import java.io.Console;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;

import org.apache.tools.ant.DefaultLogger;
import org.apache.tools.ant.Main;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;

public class MainTest1 {
	
	
	private static Path pathDirJdk = Path.of( "C:/dev23/jdk-18" );
	private static Path pathExeJava = pathDirJdk.resolve("bin/java");
	

	public static void main(String[] args) throws IOException {
		
//		execInTerminal();
		
		new MainTest1().test();
		
		
	}
	
	
	public void test() {
		
		File AntFile = new File(this.getClass().getResource("/postgresql.xml").getFile());
		
		Project p = new Project();
		
		p.setProperty( "my.prop", "Emmanuel" );
		
		var logger = new DefaultLogger();
		logger.setErrorPrintStream( System.err );
		logger.setOutputPrintStream( System.out );
		logger.setMessageOutputLevel(  Project.MSG_INFO );
		p.addBuildListener( logger );
		
//		p.setUserProperty(
//		    "ant.file", 
//		    AntFile.toString()
//		    );

		p.init();

		ProjectHelper helper = ProjectHelper.getProjectHelper();
		p.addReference("ant.projectHelper", helper);
		helper.parse(p, AntFile);
		
		
		
//		p.executeTarget(p.getDefaultTarget());
//		p.executeTarget( "client1" );
//		p.executeTarget( "start-with-debug" );
		p.executeTarget( "stop" );
			
	}
	
	
	private void execAntTarget( String  target ) {
		
		File AntFile = new File(this.getClass().getResource("/postgresql.xml").getFile());
		
		Project p = new Project();
		
		p.setProperty( "my.prop", "Emmanuel" );
		
		var logger = new DefaultLogger();
		logger.setErrorPrintStream( System.err );
		logger.setOutputPrintStream( System.out );
		logger.setMessageOutputLevel(  Project.MSG_INFO );
		p.addBuildListener( logger );
		
		p.setUserProperty(
		    "ant.file", 
		    AntFile.toString()
		    );

		p.init();

		ProjectHelper helper = ProjectHelper.getProjectHelper();
		p.addReference("ant.projectHelper", helper);
		helper.parse(p, AntFile);
		
		p.executeTarget( target );
	}
	
	
	
	public static void execInTerminal() throws IOException {
		
		if ( System.getProperty("os.name").startsWith( "Windows") ) {
			Runtime.getRuntime().exec(new String[]{
					"CMD","/C",
					"START","CMD","/K",
					pathExeJava.toString(),
					"-classpath", 
					"org.apache.tools.ant.launch.Launcher",
					"client1"
			});
		}
		
	}
	
	
    public static void exec () throws IOException, InterruptedException, URISyntaxException{
    	
        Console console = System.console();
        if(console == null && !GraphicsEnvironment.isHeadless()){
            String filename = Main.class.getProtectionDomain().getCodeSource().getLocation().toString().substring(6);
            Runtime.getRuntime().exec(new String[]{"cmd","/c","start","cmd","/k","java -jar \"" + filename + "\""});
        }else{
//            THEMAINCLASSNAMEGOESHERE.main(new String[0]);
            System.out.println("Program has ended, please type 'exit' to close the console");
        }
    }
	
	
	public static void infos()
    {
        String nameOS = "os.name";        
        String versionOS = "os.version";        
        String architectureOS = "os.arch";
        System.out.println("\n    The information about OS");
        System.out.println("\nName of the OS: " + 
        System.getProperty(nameOS));
        System.out.println("Version of the OS: " + 
        System.getProperty(versionOS));
        System.out.println("Architecture of THe OS: " + 
        System.getProperty(architectureOS));
    }	
	

}
