package ant;

public class Executor {

	public static void main(String[] args) {

        CommandLineParser clp = new CommandLineParser(args);

        String name = clp.getArgumentValue("name");
        
        int prints = Integer.parseInt(clp.getArgumentValue("prints"));
        boolean isTest = clp.getFlag("test");

        if(isTest)
            System.out.println("This is only a test");

        for(int i = 0; i < prints; i++)
        {
            System.out.print(name+" ");
        }        
        
	}

}
