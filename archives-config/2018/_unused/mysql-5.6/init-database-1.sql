
-- Supprime le compte anonyme local
DROP USER  ''@'localhost' ;

-- Supprime les comptes root non utiles
DROP USER  'root'@'127.0.0.1' ;
DROP USER  'root'@'::1' ;

-- Change le mot de passe du compte root
-- UPDATE mysql.user SET Password=PASSWORD('adminadmin') WHERE User='root';

-- Cr�e un utilisateur admin
CREATE USER 'admin'@'%' IDENTIFIED BY 'adminadmin';
GRANT ALL ON *.* TO 'admin'@'%' WITH GRANT OPTION;

FLUSH PRIVILEGES;
