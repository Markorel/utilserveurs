@ECHO OFF
SETLOCAL
CHCP 65001 >NUL
CD /D "%~dp0"

SET FULLPATH_ROOT=C:\dev25
SET  PATH_DIR_JDK=jdk-22xx

SET     JAVA_HOME=%FULLPATH_ROOT%\%PATH_DIR_JDK%
SET  DBEAVER_HOME=%FULLPATH_ROOT%\DBeaver
::SET  UTILSVR_HOME=%FULLPATH_ROOT%\UtilServeurs
SET  UTILSVR_HOME=.
SET  ELEVATE_HOME=%UTILSVR_HOME%\lib

IF NOT EXIST "%JAVA_HOME%" (
  SET   JAVA_HOME=%UTILSVR_HOME%\lib\jre
)

SET MODULEPATH=^
%UTILSVR_HOME%\lib\appli;^
%UTILSVR_HOME%\lib\modulepath

SET  CLASSPATH=^
config;%UTILSVR_HOME%\config;^
%UTILSVR_HOME%\lib\drivers\*;^
%UTILSVR_HOME%\lib\classpath\*

SET    ARGS_VM=^
 --module-path "%MODULEPATH%"^
 --class-path "%CLASSPATH%"

SET ARGS_APPLI=^
 -Dpath.root.softs="%FULLPATH_ROOT%"^
 -Dpath.dir.jdk="%JAVA_HOME%"^
 -Dpath.exe.dbeaver="%DBEAVER_HOME%\dbeaver"^
 -Dpath.exe.elevate="%ELEVATE_HOME%\elevate"

START "" "%JAVA_HOME%\bin\javaw.exe" %ARGS_VM% -m utilserveurs %ARGS_APPLI%
::"%JAVA_HOME%\bin\java.exe" %ARGS_VM% -m utilserveurs %ARGS_APPLI%

ENDLOCAL
