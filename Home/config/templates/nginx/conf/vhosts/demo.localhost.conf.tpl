server {
	listen ${server.port};
	server_name demo.localhost ;
	root   "${path.dir.sites}/demo";
    
	index index.html index.htm index.php;

	# Access Restrictions
	allow       127.0.0.1;
	deny        all;

	location / {
		try_files $uri $uri/ =404;
		autoindex on;
	}

	location ~ \.php$ {
            fastcgi_pass   127.0.0.1:${fastcgi.port};
            fastcgi_index  index.php;
            include        fastcgi.conf;
	}

	charset utf-8;

	location = /favicon.ico { access_log off; log_not_found off; }
	location = /robots.txt  { access_log off; log_not_found off; }
	location ~ /\.ht {
		deny all;
	}
	
}