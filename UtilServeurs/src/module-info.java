module utilserveurs {
	
//	exports utilserveurs to javafx.graphics;
	exports utilserveurs.gui to jfox;
	exports utilserveurs.task to ant;

	opens utilserveurs to javafx.graphics;
	opens utilserveurs.gui.view to jfox, javafx.fxml;
	opens utilserveurs.model to jfox;
	
	requires jfox;

	requires java.sql;
	requires java.desktop;

	requires javafx.base;
	requires javafx.controls;
	requires javafx.fxml;
	requires javafx.graphics;

	requires jakarta.annotation;
	requires jakarta.inject;
	
	requires ant;

}