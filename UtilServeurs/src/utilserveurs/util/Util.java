package utilserveurs.util;

import static java.nio.file.FileVisitOption.FOLLOW_LINKS;
import static java.nio.file.FileVisitResult.CONTINUE;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.EnumSet;
import java.util.Map;
import java.util.Properties;

import org.apache.tools.ant.DefaultLogger;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;
import org.apache.tools.ant.Target;

import jfox.javafx.util.UtilFX;
import utilserveurs.task.Browser;
import utilserveurs.task.Kill;
import utilserveurs.task.Shell;
import utilserveurs.task.Terminal;

public class Util {

	// -------
	// Constantes
	// -------

	public static final String DIR_SERVERS		= "servers";
	public static final String DIR_SCRIPTS		= "scripts";
	public static final String DIR_TEMPLATES	= "templates";
	public static final String PROPS_LOGIING	=  "logging.properties";
	public static final String PROPS_GLOBAL		= "global.properties";

	public static final String KEY_PATH_DIR_CURRENT		= "path.dir.current";
	public static final String KEY_PATH_DIR_JDK			= "path.dir.jdk";
	public static final String KEY_PATH_ROOT_SOFTS		= "path.root.softs";
	public static final String KEY_PATH_ROOT_DATA		= "path.root.data";
	public static final String KEY_PATH_ROOT_BACKUP		= "path.root.backup";
	public static final String KEY_PATH_DIR_SOFT		= "path.dir.soft";
	public static final String KEY_PATH_DIR_DATA		= "path.dir.data";
	public static final String KEY_FLAG_NO_DATA			= "flag.no.data";
	public static final String KEY_PATH_DIR_BACKUP		= "path.dir.backup";
	public static final String KEY_SERVER_NAME			= "server.name";
	public static final String KEY_SERVER_CLASS			= "server.class";
	public static final String KEY_SERVER_USER			= "server.user";
	public static final String KEY_SERVER_PASSWORD		= "server.password";
	public static final String KEY_SERVER_HOST			= "server.host";
	public static final String KEY_SERVER_PORT			= "server.port";
	public static final String KEY_SERVER_URL			= "server.url";
	public static final String KEY_SERVER_SERVICE		= "server.service";
	public static final String KEY_HTTP_HDR_SERVER		= "http.header.server";
	public static final String KEY_HTTP_HDR_ID			= "http.header.id";
	public static final String KEY_UTILSERVEURS_ID		= "utilserveurs.id";
	public static final String KEY_JDBC_DRIVER			= "jdbc.driver";
	public static final String KEY_JDBC_URL				= "jdbc.url";
	public static final String KEY_FLAG_COPY_FIRST		= "flag.copy.template.irst";

	public static final String KEY_DUR_REFRESH_STATUS	= "duration.refresh.status";
	public static final String KEY_DUR_WAIT_START		= "duration.wait.starting";
	public static final String KEY_DUR_WAIT_STOP		= "duration.wait.stopping";

	public static final String KEY_LEVEL_ANT_LOGGER		= "level.ant.logger";

	public static final String KEY_SCRIPT				= "script";
	public static final String KEY_NAME_DIR_TEMPLATE	= "name.dir.template";
	public static final String KEY_BACKUP_SUBPATHS		= "backup.list.subpaths";

	public static final String KEY_TARGET_CLIENT1		= "target.client1";
	public static final String KEY_TARGET_CLIENT2		= "target.client2";
	public static final String KEY_TARGET_CLIENT3		= "target.client3";
	public static final String KEY_TARGET_CLIENT4		= "target.client4";

	
	// -------
	// Actions sur les properties
	// -------

	public static void normalizePaths( final Properties props ) {
		for( var entry : props.entrySet() ) {
			if ( ( (String) entry.getKey() ).startsWith( "path." ) ) {
				var value = Path.of( props.getProperty( (String) entry.getKey() ) ).normalize().toString();
				value = value.replace( '\\', '/' );
				entry.setValue( value );
			}
		}
	}
	
	
	// -------
	// Accès aux ressources duClassPath
	// -------

	public static Path getPath(String stringPath) {
		try {
			var url = Thread.currentThread().getContextClassLoader().getResource(stringPath);
			if (url == null) {
				return null;
			}
			return Paths.get(url.toURI());
		} catch (URISyntaxException e) {
			throw UtilFX.runtimeException(e);
		}
	}

	public static Path getPathScript(String script) {
		return getPath(Util.DIR_SCRIPTS + '/' + script);
	}

	public static Path getPathTemplate(String template) {
		return getPath(Util.DIR_TEMPLATES + '/' + template);
	}

	public static InputStream getInputStream(String stringPath) {
		try {
			var url = Thread.currentThread().getContextClassLoader().getResource(stringPath);
			if (url == null) {
				throw new FileNotFoundException(stringPath + " not found in classpath.");
			}
			return url.openStream();
		} catch (IOException e) {
			throw UtilFX.runtimeException(e);
		}
	}

	public static Properties getProperties(String stringPath) {
		try (var in = getInputStream(stringPath)) {
			var props = new Properties();
			props.load(in);
			return props;
		} catch (IOException e) {
			throw UtilFX.runtimeException(e);
		}
	}

	// -------
	// Actions sur les dossiers
	// -------

	public static void deleteDir(Path path) {

		try {

			if (Files.notExists(path)) {
				return;
			}

			Files.walkFileTree(path, new SimpleFileVisitor<Path>() {

				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					Files.delete(file);
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
					Files.delete(dir);
					return FileVisitResult.CONTINUE;
				}
			});

		} catch (IOException e) {
			throw UtilFX.runtimeException(e);
		}
	}

	public static void copyDirectory(final Path pathSource, final Path pathTarget) {

		final var visitor = new SimpleFileVisitor<Path>() {

			@Override
			public FileVisitResult preVisitDirectory(final Path dir, final BasicFileAttributes attrs) {
				try {
					Files.createDirectories(pathTarget.resolve(pathSource.relativize(dir)));
					return FileVisitResult.CONTINUE;
				} catch (IOException e) {
					throw UtilFX.runtimeException(e);
				}
			}

			@Override
			public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) {
				try {
					Files.copy(file, pathTarget.resolve(pathSource.relativize(file)), REPLACE_EXISTING);
					return CONTINUE;
				} catch (IOException e) {
					throw UtilFX.runtimeException(e);
				}
			}
		};

		try {
			Files.walkFileTree(pathSource, EnumSet.of(FOLLOW_LINKS), Integer.MAX_VALUE, visitor);
		} catch (IOException e) {
			throw UtilFX.runtimeException(e);
		}
	}

	// -------
	// Ant
	// -------

	public static void execAntTarget(Path pathScript, String target, Properties props) {

		var project = new Project();

        int level = 0;
		try {
			var propLevel = props.getProperty( KEY_LEVEL_ANT_LOGGER, "WARN" );
			var field = Project.class.getDeclaredField(  "MSG_" + propLevel );
			field.setAccessible(true);
			level =  field.getInt(null);
		} catch (NoSuchFieldException e) {
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
        
		var logger = new DefaultLogger();
		logger.setErrorPrintStream(System.err);
		logger.setOutputPrintStream(System.out);
		logger.setMessageOutputLevel( level );
		project.addBuildListener(logger);
		
		project.addTaskDefinition("kill", Kill.class );
		project.addTaskDefinition("browser", Browser.class );
		project.addTaskDefinition("shell", Shell.class );
		project.addTaskDefinition("terminal", Terminal.class );
		
		for (var key : props.keySet()) {
			project.setUserProperty((String) key, props.getProperty((String) key));
		}
		project.setUserProperty( "ant.file", pathScript.toString() );
		
		project.init();

		ProjectHelper helper = ProjectHelper.getProjectHelper();
//		project.addReference("ant.projectHelper", helper);
		helper.parse(project, pathScript.toFile());

		project.executeTarget(target);
	}

	public static Map<String, Target> getTargets(Path pathScript) {
		Project p = new Project();
		p.init();
		ProjectHelper.getProjectHelper().parse(p, pathScript.toFile());
		return p.getTargets();

	}

}
