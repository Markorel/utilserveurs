package utilserveurs.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.function.Consumer;

public class StreamGobbler implements Runnable {
	
	//-------
	// Champs
	//-------
	
    private InputStream		inputStream;
    private Consumer<String> consumer;

    //-------
    // Constructeur 
    //-------
    
    public StreamGobbler(InputStream inputStream, Consumer<String> consumer) {
        this.inputStream = inputStream;
        this.consumer = consumer;
    }

    //-------
    // run()
    //-------
    
    @Override
    public void run() {
        new BufferedReader(new InputStreamReader(inputStream)).lines()
          .forEach(consumer);
    }
}