package utilserveurs.model;

public enum EnumStatusData {
	
	// Eléments
	
	PRESENT,
	ABSENT,
	UNKNOWN;

}
