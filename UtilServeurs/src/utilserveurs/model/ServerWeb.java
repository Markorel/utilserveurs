package utilserveurs.model;

import static utilserveurs.model.EnumStatusServer.STARTED;
import static utilserveurs.model.EnumStatusServer.STOPPED;
import static utilserveurs.util.Util.KEY_HTTP_HDR_ID;
import static utilserveurs.util.Util.KEY_HTTP_HDR_SERVER;
import static utilserveurs.util.Util.KEY_SERVER_URL;

import java.net.HttpURLConnection;
import java.net.URI;
import java.nio.file.Path;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.logging.Level;
import java.util.logging.Logger;

import utilserveurs.util.XProperties;

public class ServerWeb extends Server {

	//-------
	// Logger
	//-------

	private final static Logger logger = Logger.getLogger( ServerWeb.class.getName() );
	
	//-------
	// Champs
	//-------
	
	private URI		uri;
	private String 	propServer;
	private String 	propId;
	
	
	//-------
	// Constructeur
	//-------
	
	public ServerWeb( XProperties props ) throws Exception {
		super( props );
		uri = new URI( props.getProperty(KEY_SERVER_URL) );
		propServer	= props.getProperty(KEY_HTTP_HDR_SERVER, "" );
		propId		= props.getProperty(KEY_HTTP_HDR_ID, "" );
		if (  propId != null ) {
			propId = normalize( Path.of( propId ).normalize().toString() );
			props.setProperty(KEY_HTTP_HDR_ID, propId );
		}
	}

	
	//-------
	// Actions 
	//-------
	
	@Override
	public EnumStatusServer pingServer() {

		var status = STOPPED;
		
		try {
			
			var conn = (HttpURLConnection) uri.toURL().openConnection();
			
			if ( flagLogPing && logger.isLoggable( Level.FINE ) )  {
				for ( String key : conn.getHeaderFields().keySet()) {
					System.out.printf( "%-15s %s \n", key, conn.getHeaderField(key) );
				}
			}
			
			if ( conn.getHeaderFields().size() != 0 ) {
				
				String headerServer = conn.getHeaderField("Server");
				String headerId		= conn.getHeaderField("Utilserveurs-Id");;
				if( headerServer == null ) {
					headerServer = "";
				}
				if( headerId == null ) {
					headerId = "";
				} else {
					headerId = normalize( headerId );
				}
				
				if ( ! propServer.isEmpty() && ! headerServer.startsWith( propServer ) ) {
				} else if ( ! propId.isEmpty() && ! headerId.startsWith( propId )  ) {
				} else {
					status =  STARTED;
				}

				if ( flagLogPing )  {
					StringBuilder sb = new StringBuilder();
					sb.append( name );
					sb.append("\n    Expected : " + propServer);
					sb.append("\n    Received : " + headerServer );
					sb.append("\n    Expected : " + propId);
					sb.append("\n    Received : " + headerId );
					logger.config( sb.toString() );
				}
			}
			
		} catch (Exception e) {
			status = STOPPED;
			e.printStackTrace();
		}
		
		return status;
	}
	
	//-------
	// Méthodes auxiliaires
	//-------

	/**
	 * Remplace les caractères accentués par des non accenués
	 * Remplace les caractères '\' par des  '/'
	 * Nécessaire car les caractères accentués posent souvent problè_me 
	 * dans les HTTP Headers
	 * @param text
	 * @return
	 */
	private String normalize( String text ) {
		return Normalizer.normalize(text, Form.NFD)
				.replaceAll("\\p{M}", "")
				.replace("\\", "/");
	}

}
