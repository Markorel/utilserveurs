package utilserveurs.model;

import static utilserveurs.model.EnumStatusServer.STARTED;
import static utilserveurs.model.EnumStatusServer.STOPPED;
import static utilserveurs.model.EnumStatusServer.UNKNOWN;
import static utilserveurs.util.Util.KEY_JDBC_DRIVER;
import static utilserveurs.util.Util.KEY_JDBC_URL;
import static utilserveurs.util.Util.KEY_SERVER_PASSWORD;
import static utilserveurs.util.Util.KEY_SERVER_USER;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.logging.Level;
import java.util.logging.Logger;

import jfox.jdbc.UtilJdbc;
import utilserveurs.util.XProperties;

public class ServerDatabase extends Server {

	//-------
	// Logger
	//-------

	private final static Logger logger = Logger.getLogger( ServerDatabase.class.getName() );

	
	//-------
	// Champs
	//-------
	
	protected String		url;
	protected String		user;
	protected String		password;
	protected Class<?>		classDriver;
	
	protected Connection	connection;
	
	
	//-------
	// Constructeur
	//-------
	
	public ServerDatabase( XProperties props ) {
		super(props);
		url = props.getProperty(KEY_JDBC_URL);
		user = props.getProperty(KEY_SERVER_USER);
		password = props.getProperty(KEY_SERVER_PASSWORD);
		try {
			classDriver = Class.forName(props.getProperty(KEY_JDBC_DRIVER));
		} catch (ClassNotFoundException e) {
			logger.log( Level.SEVERE, e.getMessage(), e );
		}
	}

	
	//-------
	// Actions
	//-------
	
	@Override
	public EnumStatusServer pingServer() {
		
		if ( classDriver == null ) {
			return UNKNOWN;
		}
		
		var status = STOPPED;

		try {
			if ( connection == null ) {
				connection = DriverManager.getConnection(url, user, password);
			}
			if ( ! connection.isValid(0) ) {
			} else 	if ( ! testconnectionValidity() ) {
			} else {
				status = STARTED;
			}
		} catch (Exception e) {
			close();
			logger.log( Level.FINE, e.getMessage(), e);
		} finally {
			if ( status == STOPPED ) {
				close();
			}
		}
		
		return status;
		
	}


	@Override
	public void close() {
		UtilJdbc.close( connection );
		connection = null;
	}

	
	//-------
	// Méthodes auxiliaires
	//-------
	
	protected boolean testconnectionValidity() throws Exception {
		return true;
	}
	
}
