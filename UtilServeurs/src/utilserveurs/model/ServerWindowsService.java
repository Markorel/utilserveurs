package utilserveurs.model;

import static utilserveurs.model.EnumStatusData.ABSENT;
import static utilserveurs.model.EnumStatusData.PRESENT;
import static utilserveurs.model.EnumStatusServer.STARTED;
import static utilserveurs.model.EnumStatusServer.STOPPED;
import static utilserveurs.model.EnumStatusServer.UNAVAILABLE;
import static utilserveurs.util.Util.KEY_SERVER_SERVICE;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.Executors;

import utilserveurs.util.StreamGobbler;
import utilserveurs.util.XProperties;

public class ServerWindowsService extends Server {
	
	//-------
	// Champs
	//-------
	
	private String 	service;

	//-------
	// Constructeur 
	//-------
	
	public ServerWindowsService(XProperties props) {
		super(props);
	}

	//-------
	// Actions 
	//-------
	
	@Override
	public void testStatusData() {

		int exitCode = 0;
		service	= props.getProperty( KEY_SERVER_SERVICE, "" );
		
		try {
			var executor = Executors.newSingleThreadExecutor();
			var p = Runtime.getRuntime().exec( new String[] {  "sc", "query", service });
			var streamGobbler = new StreamGobbler( p.getInputStream(), null );
			executor.submit(streamGobbler);
			exitCode = p.waitFor();
		} catch (IOException | InterruptedException e ) {
			throw new RuntimeException(e); 
		}

		if ( exitCode == 0 ) {
			if(  statusData != PRESENT ) {
				statusData = PRESENT;
				statusServer = STOPPED;
			}
		} else {
			if(  statusData != ABSENT ) {
				statusData = ABSENT;
				statusServer = UNAVAILABLE;
			}
		}
	}
	
	@Override
	public EnumStatusServer pingServer() {

		var status = STOPPED;
		
		try {
			
			var p = Runtime.getRuntime().exec( new String[] {  "sc", "query", service });
			var reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			var line = reader.readLine();
			while (line != null) {
				if (line.trim().startsWith("STATE")) {
					if (line.trim().substring(line.trim().indexOf(":") + 1, line.trim().indexOf(":") + 4).trim().equals("4")) {
						status =  STARTED;
					}
				}
				line = reader.readLine();
			}
		} catch (IOException e1) {
			throw new RuntimeException(e1); 
		}
		return status;
	}

}
