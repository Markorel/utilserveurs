package utilserveurs.model;

import static java.nio.file.FileVisitResult.CONTINUE;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static java.util.logging.Level.FINE;
import static utilserveurs.model.EnumStatusData.ABSENT;
import static utilserveurs.model.EnumStatusData.PRESENT;
import static utilserveurs.model.EnumStatusServer.BUSY;
import static utilserveurs.model.EnumStatusServer.STARTED;
import static utilserveurs.model.EnumStatusServer.STARTING;
import static utilserveurs.model.EnumStatusServer.STOPPED;
import static utilserveurs.model.EnumStatusServer.STOPPING;
import static utilserveurs.model.EnumStatusServer.UNAVAILABLE;
import static utilserveurs.model.EnumStatusServer.UNKNOWN;
import static utilserveurs.util.Util.KEY_BACKUP_SUBPATHS;
import static utilserveurs.util.Util.KEY_DUR_REFRESH_STATUS;
import static utilserveurs.util.Util.KEY_DUR_WAIT_START;
import static utilserveurs.util.Util.KEY_DUR_WAIT_STOP;
import static utilserveurs.util.Util.KEY_FLAG_COPY_FIRST;
import static utilserveurs.util.Util.KEY_FLAG_NO_DATA;
import static utilserveurs.util.Util.KEY_NAME_DIR_TEMPLATE;
import static utilserveurs.util.Util.KEY_PATH_DIR_BACKUP;
import static utilserveurs.util.Util.KEY_PATH_DIR_DATA;
import static utilserveurs.util.Util.KEY_PATH_ROOT_BACKUP;
import static utilserveurs.util.Util.KEY_SCRIPT;
import static utilserveurs.util.Util.KEY_SERVER_NAME;

import java.io.IOException;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import jfox.exception.ExceptionAnomaly;
import utilserveurs.util.Util;
import utilserveurs.util.XProperties;


public abstract class Server {

	//-------
	// Logger
	//-------

	private final static Logger logger = Logger.getLogger( Server.class.getName() );

	//-------
	// Constantes
	//-------
	
	public static final String TARGET_START		= "start";
	public static final String TARGET_START_DBG	= "start-with-debug";
	public static final String TARGET_STOP		= "stop";
	public static final String TARGET_KILL		= "kill";
	public static final String TARGET_CLIENT1	= "client1";
	public static final String TARGET_CLIENT2	= "client2";
	public static final String TARGET_CLIENT3	= "client3";
	public static final String TARGET_CLIENT4	= "client4";
	public static final String TARGET_CREATE	= "create-data";

	
	//-------
	// Champs observable
	//-------
	
	protected ObjectProperty<EnumStatusServer> propStatusServer = new SimpleObjectProperty<>( EnumStatusServer.UNKNOWN );

	protected ObjectProperty<EnumStatusData> propStatusData = new SimpleObjectProperty<>( EnumStatusData.UNKNOWN );
	
	
	//-------
	// Autres champs
	//-------
	
	protected XProperties		props;
	protected String			name;
	protected Path				pathScript;
	protected boolean			flagNoData;
	protected Path				pathDirData;
	protected Path				pathDirBackup;
	private String				targetClient1;
	private String				targetClient2;
	private String				targetClient3;
	private String				targetClient4;
	protected boolean			flagCreate;
	protected boolean			flagKill;
	protected boolean			flagClient1;
	protected boolean			flagClient2;
	protected boolean			flagClient3;
	protected boolean			flagClient4;
	protected String			textToolTipClient1;
	protected String			textToolTipClient2;
	protected EnumStatusServer	statusServer = EnumStatusServer.UNKNOWN;
	protected EnumStatusData	statusData = EnumStatusData.UNKNOWN;
	protected boolean			flagLogPing = true;
	protected int				counterStarting = 0;
	protected int				counterStopping = 0;
	protected int				durationRefreshStatus = 1500;
	protected int				durationWaitStarting = 10000;
	protected int				durationWaitStopping = 10000;
	
	//-------
	// Getters et setters
	//-------
	
	public ObjectProperty<EnumStatusServer> statusServerProperty() {
		return propStatusServer;
	}
	
	public ObjectProperty<EnumStatusData> statusDataProperty() {
		return propStatusData;
	}
	
	public EnumStatusServer getStatusServer() {
		return propStatusServer.get();
	}
	
	public EnumStatusData getStatusData() {
		return propStatusData.get();
	}
	
	public String getName() {
		return name;
	}

	public boolean isFlagNoData() {
		return flagNoData;
	}
	
	public Path getPathDirData() {
		return pathDirData;
	}
	
	public Path getPathDirBackup() {
		return pathDirBackup;
	}
	
	public boolean isFlagKill() {
		return flagKill;
	}
	
	public boolean isFlagClient1() {
		return flagClient1;
	}
	
	public boolean isFlagClient2() {
		return flagClient2;
	}
	
	public String getTextToolTipClient1() {
		return textToolTipClient1;
	}
	
	public String getTextToolTipClient2() {
		return textToolTipClient2;
	}
	
	public String getProperty( String name ) {
		return props.getProperty(name);
	}
	
	
	//-------
	// Constructeur
	//-------
	
	public Server( XProperties props ) {

		this.props = props;
		
		// Nom
		name = props.getProperty(KEY_SERVER_NAME);
		
		// Durées d'attentes
		durationRefreshStatus =props.getInt(KEY_DUR_REFRESH_STATUS, durationRefreshStatus);
		durationWaitStarting = props.getInt(KEY_DUR_WAIT_START, durationWaitStarting);
		durationWaitStopping = props.getInt(KEY_DUR_WAIT_STOP, durationWaitStopping);

		String stringPath;
		
		// Script
		pathScript = Util.getPathScript( props.getProperty( KEY_SCRIPT ) );
		if (pathScript == null  ) {
			throw new ExceptionAnomaly( "impossible de trouver le script " + props.getProperty( KEY_SCRIPT ) );
		}
		
		// Dossier DATA
		flagNoData = props.getBoolean( KEY_FLAG_NO_DATA, false );
		if( ! flagNoData) {
			stringPath = props.getProperty(KEY_PATH_DIR_DATA);
			pathDirData = Paths.get( stringPath ).normalize();
			logger.config( "path.dir.data : " + pathDirData.toString() );
		}
		testStatusData();
		
		// Dossier backup
		if ( ! flagNoData ) {
			stringPath = props.getProperty(KEY_PATH_DIR_BACKUP);
			if( stringPath != null ) {
				pathDirBackup = Paths.get( stringPath ).normalize();
			} else {
				stringPath = props.getProperty(KEY_PATH_ROOT_BACKUP);
				pathDirBackup = Paths.get( stringPath ).normalize().resolve(pathDirData.getFileName());
			}
		}

		// Targets cliens
		targetClient1 = props.getProperty( Util.KEY_TARGET_CLIENT1, TARGET_CLIENT1 );
		targetClient2 = props.getProperty( Util.KEY_TARGET_CLIENT2, TARGET_CLIENT2 );
		targetClient3 = props.getProperty( Util.KEY_TARGET_CLIENT3, TARGET_CLIENT3 );
		targetClient4 = props.getProperty( Util.KEY_TARGET_CLIENT4, TARGET_CLIENT4 );
		
		// Flags targets
		var targets = Util.getTargets(pathScript);
		flagCreate  = targets.containsKey( TARGET_CREATE ); 
		flagKill	= targets.containsKey( TARGET_KILL ); 
		flagClient1 = ! targetClient1.isEmpty() && targets.containsKey( targetClient1 ); 
		flagClient2 = ! targetClient2.isEmpty() && targets.containsKey( targetClient2 ); 
		flagClient3 = ! targetClient3.isEmpty() && targets.containsKey( targetClient3 ); 
		flagClient4 = ! targetClient4.isEmpty() && targets.containsKey( targetClient4 ); 

		if( flagClient1 ) {
			textToolTipClient1 = targets.get( targetClient1 ).getDescription();
			if( flagClient3 ) {
				textToolTipClient1 += "\n[Ctrl] " + targets.get( targetClient3 ).getDescription();
			}
		}
		if( flagClient2 ) {
			textToolTipClient2 = targets.get( targetClient2 ).getDescription();
			if( flagClient4 ) {
				textToolTipClient2 += "\n[Ctrl] " + targets.get( targetClient4 ).getDescription();
			}
		}
		
	}
	
	
	//-------
	// Méthodes abstraites
	//-------
	
	public abstract EnumStatusServer pingServer();
	
	
	//-------
	// Actions
	//-------

	
	public void testStatusServer() {
		
		if ( statusServer == UNAVAILABLE
			|| statusServer == BUSY ) {
			return;
		}
		
		if ( statusData == ABSENT ) {
			statusServer = UNAVAILABLE;
			return;
		}
		
		var status = pingServer();
		flagLogPing = false;
		
		if ( counterStarting > 0 ) {
			if ( status == STARTED ) {
				counterStarting = 0;
			} else {
				counterStarting -= durationRefreshStatus;
				return;
			} 
		}
		if ( counterStopping > 0 ) {
			if ( status == STOPPED ) {
				counterStopping = 0;
			} else {
				counterStopping -= durationRefreshStatus;
				return;
			}
		}
		flagLogPing= statusServer != status;
		statusServer = status;
	}
	
	public void refreshPropStatus() {
		propStatusServer.set(statusServer);
		propStatusData.set(statusData);
	}
	
	
	public void testStatusData() {
		if ( Files.exists(pathDirData) && Files.isDirectory(pathDirData) ) {
			if(  statusData != PRESENT ) {
				statusData = PRESENT;
				statusServer = STOPPED;
			}
		} else {
			if(  statusData != ABSENT ) {
				statusData = ABSENT;
				statusServer = UNAVAILABLE;
			}
		}
	}
	
	
	public void start( boolean flagWithDebug ) throws Exception {
		statusServer = STARTING;
		counterStarting = durationWaitStarting;
		var target = flagWithDebug ? TARGET_START_DBG : TARGET_START; 
		Util.execAntTarget( pathScript, target, props );
	}

	public void stop() throws Exception {
		statusServer = STOPPING;
		counterStopping = durationWaitStopping;;
		Util.execAntTarget( pathScript, TARGET_STOP, props );
	}

	public void kill() {
		if ( flagKill ) {
			statusServer = STOPPING;
			counterStopping = durationWaitStopping;;
			Util.execAntTarget( pathScript, TARGET_KILL, props );
		}
	}
	
	
	public void runClient1(boolean flagControl) {
		if ( flagControl && flagClient3 ) {
			Util.execAntTarget( pathScript, targetClient3, props );
		} else {
			Util.execAntTarget( pathScript, targetClient1, props );
		}
	}
	
	public void runClient2(boolean flagControl) {
		if ( flagControl && flagClient4 ) {
			Util.execAntTarget( pathScript, targetClient4, props );
		} else {
			Util.execAntTarget( pathScript, targetClient2, props );
		}
	}

	
	public void dataCreate() throws Exception {
		
		testStatusData();
		if ( statusData != ABSENT  ) {
			statusServer = UNKNOWN;
			logger.log( Level.FINE, "Le dossier des données existe déjà.\n" + pathDirData.toString() );
			throw new ExceptionAnomaly();
		}

		try {
			statusServer = BUSY;
	        Files.createDirectories( pathDirData.getParent() );
			boolean flagCopyFirst = props.getBoolean( KEY_FLAG_COPY_FIRST, false );
			if ( flagCopyFirst ) {
				Files.createDirectories(pathDirData);
				copyTemplate();
			}
			if ( flagCreate ) {
				Util.execAntTarget( pathScript, TARGET_CREATE, props );
			}
			if ( ! flagCopyFirst ) {
				copyTemplate();
			}
		} catch ( Exception e) {
			statusServer = UNAVAILABLE;
			logger.log( FINE, e.getMessage(), e);
			throw new ExceptionAnomaly( e );
		}
		
		testStatusData();
		if ( statusData == PRESENT  ) {
			statusServer = STOPPED;
		} else {
			statusServer = UNAVAILABLE;
			logger.fine( "Impossible de créer le dossier " + pathDirData );
			throw new ExceptionAnomaly();
		}
		
	}

	public void dataDelete() throws Exception {
		if ( statusServer == STOPPED && statusData == PRESENT ) {
			statusServer = BUSY;
			try {
				Util.deleteDir( pathDirData );
				statusServer = UNAVAILABLE;
				statusData = ABSENT;
			} catch (Exception e) {
				statusServer = STOPPED;
				logger.log( FINE, e.getMessage(), e);
				throw new ExceptionAnomaly( e );
			}
		}
	}
	
	public void dataBackup() throws Exception {
		
		String nameArchive = pathDirData.getFileName().toString() +  
				LocalDateTime.now().format( DateTimeFormatter.ofPattern("__yyyy-MM-dd_HH-mm") );
		Path pathArchive = pathDirBackup.resolve( nameArchive + ".zip" );

		Files.createDirectories(pathArchive.getParent() );
		Files.deleteIfExists(pathArchive);
		
		URI uri = URI.create( "jar:" + pathArchive.toUri().toString() );
		final Map<String, String> env = new HashMap<>();
		env.put("create", "true");	

		try ( FileSystem fsArchive = FileSystems.newFileSystem( uri, env ) ) {
			List<String> stringSubpaths = props.getList(KEY_BACKUP_SUBPATHS);
			if ( stringSubpaths != null ) {
				for ( String stringSubpath : stringSubpaths ) {
					backup(pathDirData, Paths.get( stringSubpath ), fsArchive );
				}
			} else {
				backup(pathDirData, Paths.get( "." ), fsArchive );
			}
		}
   	}
	
	public void dataRestore( Path pathArchive ) throws Exception {

		if ( Files.notExists( pathDirData) ) {
			Files.createDirectories(pathDirData);
			statusData = PRESENT;
			statusServer = STOPPED;
		}
		
		URI uri = URI.create( "jar:" + pathArchive.toUri().toString() );
		final Map<String, String> env = new HashMap<>();

		try (FileSystem fsArchive = FileSystems.newFileSystem(uri, env);) {
			final Path root = fsArchive.getPath("/");

			// walk the zip file tree and copy files to the destination
			Files.walkFileTree(root, new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					final Path destFile = Paths.get( pathDirData.toString(), file.toString() );
					Files.copy(file, destFile, REPLACE_EXISTING);
					return CONTINUE;
				}

				@Override
				public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
					final Path dirToCreate = Paths.get( pathDirData.toString(), dir.toString() );
					if (Files.notExists(dirToCreate)) {
						Files.createDirectory(dirToCreate);
					}
					return CONTINUE;
				}
			});
		}
	}


	public void close() {
	}

	
	//-------
	// Méthodes auxiliaires
	//-------
	
	private void copyTemplate()  {
		Path pathDirTemplate = Util.getPathTemplate( props.getProperty(KEY_NAME_DIR_TEMPLATE));
		if ( pathDirTemplate != null && Files.exists(pathDirTemplate) && Files.isDirectory(pathDirTemplate) ) {
			Util.copyDirectory( pathDirTemplate, pathDirData );
		}
	}
	
	protected void backup( Path pathDirData, Path subPath, FileSystem fsArchive  ) throws IOException {
		
	    Files.walkFileTree( pathDirData.resolve( subPath ), new SimpleFileVisitor<Path>(){
           @Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				final Path dest = fsArchive.getPath("/", pathDirData.relativize(file).toString());
				Files.copy(file, dest, REPLACE_EXISTING);
				return CONTINUE;
	          }
  
           @Override
			public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
				final Path dirToCreate = fsArchive.getPath("/", pathDirData.relativize(dir).toString());
				if (Files.notExists(dirToCreate)) {
					Files.createDirectories(dirToCreate);
				}
				return CONTINUE;
	           }
         });
		
	}
}
