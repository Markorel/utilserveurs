package utilserveurs.model;

import javafx.scene.paint.Color;

public enum EnumStatusServer {
	
	// Eléments
	
	UNKNOWN		( "Grey"	),
	UNAVAILABLE	( "Black"	),
	BUSY		( "Blue"	),
	STARTING	( "Orange"	),
	STARTED		( "LimeGreen"),
	STOPPING	( "Maroon"	),
	STOPPED		( "Red"		);
	
	
	// Champs
	
	private Color color;
	
	
	// Propriétés
	
	public Color getColor() {
		return color;
	}
	
	
	// Constructeur
	
	private EnumStatusServer( String color ) {
		this.color = Color.web(color);
	}

}
