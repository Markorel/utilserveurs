package utilserveurs.model;

import static utilserveurs.util.Util.KEY_DUR_REFRESH_STATUS;
import static utilserveurs.util.Util.KEY_PATH_DIR_SOFT;
import static utilserveurs.util.Util.KEY_SERVER_CLASS;
import static utilserveurs.util.Util.KEY_SERVER_NAME;

import java.awt.Desktop;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.inject.Inject;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.Observable;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.util.Duration;
import utilserveurs.util.Util;
import utilserveurs.util.XProperties;


public class ModelServer {

	//-------
	// Constantes
	//-------

	private static final int DURATION_REFRESH = 200;

	
	//-------
	// Objets observables
	//-------
	
	// Objets pour vue Liste 
	private ObservableList<Server> 	obsListServers = FXCollections.observableArrayList(
			s ->  new Observable[] { s.statusServerProperty() } 
		);
	
	// Objets pour vue Restore 
	private ObservableList<Path> 	obsListArchives = FXCollections.observableArrayList();
	
	// Objets pour vue Info 
	private StringProperty			propInfo = new SimpleStringProperty();
	private BooleanProperty			flagDirDataUnavailable = new SimpleBooleanProperty();
	
	
	//-------
	// Autres champs
	//-------

	@Inject
	private XProperties				propsGlobal;
	
	private ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
	private int 					durationRefreshStatus = 1500;
	private Server 					serverCurrent;
	private Timeline 				timeline;

	
	//-------
	// Initialisation
	//-------
	
	@PostConstruct
	public void init() {

		durationRefreshStatus = Integer.parseInt( propsGlobal.getProperty( KEY_DUR_REFRESH_STATUS, String.valueOf(durationRefreshStatus)));
		refreshListServers();

		// Actualisaiton automatique des status des serverus
		timeline = new Timeline(  new KeyFrame(
			        Duration.millis( DURATION_REFRESH ),
			        ai -> refreshStatus() ) );		
		timeline.setCycleCount(Timeline.INDEFINITE);
		timeline.setAutoReverse(true);
		timeline.playFrom( timeline.getCycleDuration());
	}
	
	
	//-------
	// Getters 
	//-------
	
	public ObservableList<Server> getObsListServers() {
		return obsListServers;
	}
	
	public ObservableList<Path> getObsListArchives() {
		return obsListArchives;
	}
	
	public StringProperty propertyInfo() {
		return propInfo;
	}
	
	public BooleanProperty propertyDirDataUnavailable() {
		return flagDirDataUnavailable;
	}
	
	
	
	//-------
	// Actions
	//-------
	
	public void refreshListServers() {

		try {

			try ( DirectoryStream<Path> directoryStream = Files.newDirectoryStream( Util.getPath(Util.DIR_SERVERS), "*.properties" ) ) {
				var servers = new ArrayList<Server>();
				for (Path path : directoryStream) {
					var props = new XProperties();
					props.putAll(propsGlobal);
					props.load(Files.newInputStream(path));
					Util.normalizePaths(props);
					var klass = Class.forName(props.getProperty(KEY_SERVER_CLASS));
					var constructor = klass.getConstructor(XProperties.class);
					servers.add((Server) constructor.newInstance(props));
				}
				obsListServers.setAll(servers);
	        }

			FXCollections.sort( obsListServers,
		            (Comparator<Server>) ( s1, s2) -> {
		                return ( s1.getName().toUpperCase().compareTo(s2.getName().toUpperCase()));
				});
			
			if ( executor != null ) {
				executor.shutdownNow();
			}
			executor = Executors.newScheduledThreadPool( obsListServers.size() );
			for ( Server server : obsListServers ) {
				executor.scheduleAtFixedRate(
						() -> server.testStatusServer(),
						0, 
						durationRefreshStatus,
						TimeUnit.MILLISECONDS );
			}
		} catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}

	
	public void refreshListArchives( Server server ) {
		
		serverCurrent = server;

		obsListArchives.clear();
		if ( Files.exists( server.getPathDirBackup() ) && Files.isDirectory( server.getPathDirBackup()) ) {
			try ( DirectoryStream<Path> directoryStream = Files.newDirectoryStream( server.getPathDirBackup(), "*.zip" ) ) {
	            for (Path path : directoryStream) {
	            	obsListArchives.add( path );
	            }
			} catch (RuntimeException e) {
				throw e;
			} catch (Exception e) {
				throw new RuntimeException(e);
	        }
		}
	}

	
	public void refreshInfo( Server server ) {
		
		serverCurrent = server;

		flagDirDataUnavailable.set( server.isFlagNoData() || server.getStatusData() != EnumStatusData.PRESENT);
		
		StringBuilder sbInfo = new StringBuilder();
		sbInfo.append( server.getProperty(KEY_SERVER_NAME) );
		if( server.getPathDirData() != null ) {
			sbInfo.append("\n");
			sbInfo.append( "Dossier des données :\n   " );
			sbInfo.append( server.getPathDirData().toString() );
		}
		if( server.getPathDirBackup() != null ) {
			sbInfo.append("\n");
			sbInfo.append( "Dossier des sauvegardes :\n   " );
			sbInfo.append( server.getPathDirBackup().toString() );
		}
		if( server.getProperty(KEY_PATH_DIR_SOFT) != null ) {
			sbInfo.append("\n");
			sbInfo.append( "Dossier d'installation :\n   " );
			sbInfo.append( Paths.get( server.getProperty(KEY_PATH_DIR_SOFT) ).normalize().toString() );
		}

		propInfo.set( sbInfo.toString() );
	}
	
		
	public void refreshStatus() {
		for ( Server server : obsListServers ) {
			server.refreshPropStatus();
		}
	}
	
	
	public void reinitStatus() {
		for ( Server server : obsListServers ) {
			server.testStatusData();
		}
	}
	
	
	public void deleteArchive( Path path )  {
		try {
			Files.delete(path);
			obsListArchives.remove(path);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void restoreArchive( Path path ) throws Exception {
		serverCurrent.dataRestore(path);
	}
	
	public void openSystemExplorer()  {
		try {
			Desktop.getDesktop().open( serverCurrent.getPathDirData().toFile() );
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	@PreDestroy
	public void colose() {
		
		for ( Server server : obsListServers ) {
			server.close();
		}
		executor.shutdownNow();
	}
	
}
