package utilserveurs.model;

import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Logger;

import utilserveurs.util.XProperties;

public class ServerDatabaseMariadb extends ServerDatabase {

	// Logger
	private final static Logger logger = Logger.getLogger( ServerDatabaseMariadb.class.getName() );
	
	
	// Constructeur

	public ServerDatabaseMariadb( XProperties props ) throws Exception {
		super(props);
	}

	
	// Méthodes auxiliaires
	
	protected boolean testconnectionValidity() throws Exception {
		Statement stmt = null;
		ResultSet rs = null;
		String sql = "SELECT @@GLOBAL.datadir";
		try {
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			if ( rs.next() ) {
				String stringPath1 = pathDirData.toString();
				String stringPath2 = Paths.get(rs.getString(1)).normalize().toString();
				if( flagLogPing ) {
					StringBuilder sb = new StringBuilder();
					sb.append( name );
					sb.append("\n    Expected : " + stringPath1);
					sb.append("\n    Received : " + stringPath2);
					logger.config( sb.toString() );
				}
				if ( stringPath1.equals( stringPath2 ) ){
					return true;
				}
			}
			return false;
		} catch ( Exception e) {
			throw e;
		} finally {
			if ( connection != null &&  rs != null ) {
				rs.close();
			}
			if ( connection != null &&  stmt != null ) {
				stmt.close();
			}
		}
	}

}
