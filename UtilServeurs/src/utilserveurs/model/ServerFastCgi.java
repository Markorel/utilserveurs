package utilserveurs.model;

import static utilserveurs.model.EnumStatusServer.STARTED;
import static utilserveurs.model.EnumStatusServer.STOPPED;
import static utilserveurs.util.Util.KEY_SERVER_HOST;
import static utilserveurs.util.Util.KEY_SERVER_PORT;

import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.util.logging.Logger;

import jfox.javafx.util.UtilFX;
import utilserveurs.util.XProperties;

public class ServerFastCgi extends Server {

	//-------
	// Logger
	//-------

	private final static Logger logger = Logger.getLogger( ServerFastCgi.class.getName() );
	
	//-------
	// Champs
	//-------
	
	private String 	host;
	private int 	port;
	private Socket	socket;
	
	
	//-------
	// Constructeur
	//-------
	
	public ServerFastCgi( XProperties props ) throws Exception {

		super( props );
		
		host = props.getProperty(KEY_SERVER_HOST);
		port = props.getInt(KEY_SERVER_PORT);
	}

	
	//-------
	// Actions 
	//-------
	
	@Override
	public EnumStatusServer pingServer() {
		
		var status = STARTED;

		try {
		
			socket = new Socket( host, port );
			socket.close();
			
		} catch (ConnectException e) {
			status = STOPPED;
		} catch (IOException e) {
			throw UtilFX.runtimeException(e);
		}
		if( flagLogPing ) {
			StringBuilder sb = new StringBuilder();
			sb.append( name );
			sb.append("\n    Host : " + host);
			sb.append("\n    Port : " + port );
			logger.config( sb.toString() );
		}
		return status;
	}

}
