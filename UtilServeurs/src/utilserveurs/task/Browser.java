package utilserveurs.task;

import org.apache.tools.ant.Task;
import org.apache.tools.ant.taskdefs.condition.Os;

import jfox.javafx.util.UtilFX;


public class Browser extends Task {

	// -------
	// Fields
	// -------

	private String url;

	// -------
	// Getters & Setters
	// -------
	
	public void setUrl(String url) {
		this.url = url;
	}

	// -------
	// Actions
	// -------

	public void execute() {

		try {

			ProcessBuilder builder = null;
			if ( Os.isFamily( Os.FAMILY_WINDOWS ) ) {
				builder = new ProcessBuilder( "CMD", "/C", "START", url );
			} else if ( Os.isFamily( Os.FAMILY_UNIX ) ) {
			} else if ( Os.isFamily( Os.FAMILY_MAC ) ) {
			}

			if ( builder != null ) {
				builder.inheritIO();
				builder.start();
			}

		} catch ( Exception e) {
			throw UtilFX.runtimeException(e);
		}
		
	}

}
