package utilserveurs.task;

import org.apache.tools.ant.taskdefs.ExecTask;
import org.apache.tools.ant.taskdefs.condition.Os;

import jfox.javafx.util.UtilFX;


public class Kill extends ExecTask {

	// -------
	// Fields
	// -------

	private String pattern;
	private boolean terminal;

	// -------
	// Getters & Setters
	// -------
	
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public void setTerminal(boolean terminal) {
		this.terminal = terminal;
	}

	// -------
	// Actions
	// -------
	public void execute() {

		try {

			if ( Os.isFamily( Os.FAMILY_WINDOWS ) ) {
				cmdl.createArgument( true ).setValue( pattern );
				cmdl.createArgument( true ).setValue( "/IM" );
				cmdl.createArgument( true ).setValue( "/F" );
				if ( terminal ) {
					cmdl.createArgument( true ).setValue( "TASKKILL" );;
					cmdl.createArgument( true ).setValue( "/K" );;
					cmdl.createArgument( true ).setValue( "CMD" );;
					cmdl.createArgument( true ).setValue( "START" );;
					cmdl.createArgument( true ).setValue( "/C" );;
					setExecutable( "CMD" );
				} else {
					setExecutable( "TASKKILL" );
				}
			} else if ( Os.isFamily( Os.FAMILY_UNIX ) ) {
			} else if ( Os.isFamily( Os.FAMILY_MAC ) ) {
			}
			
			super.execute();

		} catch ( Exception e) {
			throw UtilFX.runtimeException(e);
		}
		
	}

}
