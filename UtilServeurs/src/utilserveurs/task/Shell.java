package utilserveurs.task;

import org.apache.tools.ant.taskdefs.ExecTask;
import org.apache.tools.ant.taskdefs.condition.Os;

import jfox.javafx.util.UtilFX;


public class Shell extends ExecTask {

	// -------
	// Actions
	// -------

	public void execute() {

		try {

			if ( Os.isFamily( Os.FAMILY_WINDOWS ) ) {
				cmdl.createArgument( true ).setValue( cmdl.getExecutable() );
				cmdl.createArgument( true ).setValue( "/C" );;
				setExecutable( "CMD" );
			} else if ( Os.isFamily( Os.FAMILY_UNIX ) ) {
			} else if ( Os.isFamily( Os.FAMILY_MAC ) ) {
			}
			
			super.execute();

		} catch ( Exception e) {
			throw UtilFX.runtimeException(e);
		}
		
		
	}

}
