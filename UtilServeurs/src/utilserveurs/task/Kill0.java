package utilserveurs.task;

import org.apache.tools.ant.Task;
import org.apache.tools.ant.taskdefs.condition.Os;

import jfox.javafx.util.UtilFX;


public class Kill0 extends Task {

	// -------
	// Fields
	// -------

	private String pattern;
	private boolean terminal;

	// -------
	// Getters & Setters
	// -------
	
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public void setTerminal(boolean terminal) {
		this.terminal = terminal;
	}

	// -------
	// Actions
	// -------

	public void execute() {

		try {

			ProcessBuilder builder = null;
			if ( Os.isFamily( Os.FAMILY_WINDOWS ) ) {
				if ( terminal ) {
					builder = new ProcessBuilder( "CMD", "/C", "START", "CMD", "/K", "TASKKILL", "/F", "/IM", pattern );
				} else {
					builder = new ProcessBuilder( "TASKKILL", "/F", "/IM", pattern );
				}
			} else if ( Os.isFamily( Os.FAMILY_UNIX ) ) {
			} else if ( Os.isFamily( Os.FAMILY_MAC ) ) {
			}

			if ( builder != null ) {
				builder.inheritIO();
				builder.redirectOutput(ProcessBuilder.Redirect.DISCARD);
				var process = builder.start();
				process.waitFor();
			}

		} catch ( Exception e) {
			throw UtilFX.runtimeException(e);
		}
		
	}

}
