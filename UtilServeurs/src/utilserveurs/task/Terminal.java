package utilserveurs.task;

import org.apache.tools.ant.taskdefs.ExecTask;
import org.apache.tools.ant.taskdefs.condition.Os;

import jfox.javafx.util.UtilFX;


public class Terminal extends ExecTask {

	// -------
	// Fields
	// -------

	private boolean keep = false;

	// -------
	// Getters & Setters
	// -------
	
	public void setKeep(boolean keep) {
		this.keep = keep;
	}

	// -------
	// Actions
	// -------
	public void execute() {

		try {

			if ( Os.isFamily( Os.FAMILY_WINDOWS ) ) {
				cmdl.createArgument( true ).setValue( cmdl.getExecutable() );
				if ( keep ) {
					cmdl.createArgument( true ).setValue( "CALL" );;
				}
				cmdl.createArgument( true ).setValue( '"' + getProject().getProperty( "server.name") + '"' );;
				cmdl.createArgument( true ).setValue( "START" );;
				cmdl.createArgument( true ).setValue( "/C" );;
				setExecutable( "CMD" );
			} else if ( Os.isFamily( Os.FAMILY_UNIX ) ) {
			} else if ( Os.isFamily( Os.FAMILY_MAC ) ) {
			}
			
			super.execute();

		} catch ( Exception e) {
			throw UtilFX.runtimeException(e);
		}
		
	}

}
