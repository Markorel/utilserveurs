package utilserveurs.gui.view;

import jakarta.inject.Inject;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import jfox.javafx.view.ControllerAbstract;
import jfox.javafx.view.IManagerGui;
import utilserveurs.model.ModelServer;


public class ViewServerInfo  extends ControllerAbstract  {

	
	// Composants de la vue
	
	@FXML
	private TextArea	textArea;
	@FXML
	private Button		buttonOpenDirData;
	
	
	// Autres champs

	@Inject
	private IManagerGui	managerGui;
	@Inject
	private ModelServer	modelServer;
	
	
	// Initialisation 

	@FXML
	public void initialize() {

		// Data binding
		textArea.textProperty().bind( modelServer.propertyInfo() );
		buttonOpenDirData.disableProperty().bind( modelServer.propertyDirDataUnavailable() );
	}
	
	
	// Actons
	
	public void doOpenDirData() {
		modelServer.openSystemExplorer();
	}
	
	public void doBack() {
		managerGui.showView( ViewServerList.class );
	}
	
}
