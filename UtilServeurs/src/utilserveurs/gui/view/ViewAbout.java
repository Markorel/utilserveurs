package utilserveurs.gui.view;

import static utilserveurs.AppliUtilServeurs.APPLI_NOM;
import static utilserveurs.AppliUtilServeurs.APPLI_VERSION;
import static utilserveurs.AppliUtilServeurs.AUTEUR;
import static utilserveurs.AppliUtilServeurs.ORGANISME;

import jakarta.inject.Inject;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import jfox.javafx.view.ControllerAbstract;
import jfox.javafx.view.IManagerGui;


public class ViewAbout extends ControllerAbstract  {

	
	// Composants de la vue
	
	@FXML
	private Label		labelTitre;
	@FXML
	private Label		labelAuteur;
	
	
	// Autres champs

	@Inject
	private IManagerGui	managerGui;
	
	
	// Initialisation 

	@FXML
	public void initialize() {

		labelTitre.setText( APPLI_NOM + " " + APPLI_VERSION );
		labelAuteur.setText( AUTEUR + "\n" + ORGANISME  );
		
	}
	
	
	// Actons
	
	public void doBack() {
		managerGui.showView( ViewServerList.class );
	}
	
}
