package utilserveurs.gui.view;

import static javafx.scene.input.MouseButton.PRIMARY;
import static utilserveurs.model.EnumStatusData.ABSENT;
import static utilserveurs.model.EnumStatusData.PRESENT;
import static utilserveurs.model.EnumStatusData.UNKNOWN;
import static utilserveurs.model.EnumStatusServer.STARTED;
import static utilserveurs.model.EnumStatusServer.STOPPED;
import static utilserveurs.model.EnumStatusServer.UNAVAILABLE;

import jakarta.inject.Inject;
import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Rectangle;
import jfox.javafx.view.ControllerAbstract;
import jfox.javafx.view.IManagerGui;
import utilserveurs.model.EnumStatusData;
import utilserveurs.model.EnumStatusServer;
import utilserveurs.model.ModelServer;
import utilserveurs.model.Server;

public class ViewServerList  extends ControllerAbstract  {

	
	//-------
	// Composants visuels
	//-------
	
	@FXML
	private ListView<Server>	listViewServers;
	@FXML
	private Button				buttonStartStop;
	@FXML
	private Button				buttonCreateDelete;
	@FXML
	private Button				buttonClient1;
	@FXML
	private Button				buttonClient2;
	@FXML
	private Button				buttonInfo;
	@FXML
	private Button				buttonBackup;
	@FXML
	private Button				buttonRestore;
	
	private Tooltip				tooltipStartStop;
	private Tooltip				tooltipClient1;
	private Tooltip				tooltipClient2;
	
	//-------
	// Autres champs
	//-------
	
	@Inject
	private IManagerGui			managerGui;
	@Inject
	private ModelServer			modelServer;

	private Server				server;
	private EnumStatusServer	statusServer;
	private EnumStatusData		statusData;

	private ChangeListener<EnumStatusServer> listenerStatusServer;
	private ChangeListener<EnumStatusData> listenerStatusData;
	
	private boolean				flagControl = false;
	
	
	//-------
	// Initialisation 
	//-------

	@FXML
	public void initialize() {

		buttonStartStop.setTooltip( tooltipStartStop = new Tooltip() );
		buttonClient1.setTooltip( tooltipClient1 = new Tooltip() );
		buttonClient2.setTooltip( tooltipClient2 = new Tooltip() );

		tooltipStartStop.setText( "Start/Stop\n[Ctrl] Start with Debug\n[Alt] Kill Processes" );
		
		listenerStatusServer = (obs, oldVal, newVal ) -> configureButtons() ;
		listenerStatusData = (obs, oldVal, newVal ) -> configureButtons() ;
		
		listViewServers.setItems( modelServer.getObsListServers() );
		
		listViewServers.setCellFactory( listView -> new CellServer() );
		
		listViewServers.getSelectionModel().selectedItemProperty().addListener(
				(obs, oldVal, newVal) -> {
					server =newVal;
					if ( oldVal != null ) {
						oldVal.statusServerProperty().removeListener(listenerStatusServer);
						oldVal.statusDataProperty().removeListener(listenerStatusData);
					}
					if ( newVal != null ) {
						newVal.statusServerProperty().addListener(listenerStatusServer);
						newVal.statusDataProperty().addListener(listenerStatusData);
					}
			    	configureButtons();
				});
		
    	configureButtons();
	}
	
	
	@Override
	public void refresh() {
		modelServer.reinitStatus();
	}
	
	
	//-------
	// Actions
	//-------
	
	public void doStartStop() {
		
		if ( server.getStatusServer() == STOPPED ) {
			server.testStatusData();
			if ( server.getStatusData() == PRESENT ) {
				managerGui.execTask( () -> {
					server.start( flagControl );
				} );
			}
		} else if ( server.getStatusServer() == STARTED ) {
			managerGui.execTask( () -> {
				server.stop();
			} );
		}
 	}
	
	
	public void doClient1() {
		managerGui.execTask( () -> {  
			server.runClient1( flagControl );
		} );
	}
	
	
	public void doClient2() {
		managerGui.execTask( () -> {  
			server.runClient2( flagControl );
		} );
	}
	
	public void doInfo() {
		modelServer.refreshInfo(  listViewServers.getSelectionModel().getSelectedItem() );
		managerGui.showView( ViewServerInfo.class );
	}
	
	public void doAbout() {
		managerGui.showView( ViewAbout.class );;
	}
	
	public void doCreateDelete() {

		if (statusData == ABSENT) {
			managerGui.execTask( () -> {
				server.dataCreate();
				configureButtons();
			} );
		} else if (statusData == PRESENT) {
			if (managerGui.showDialogConfirm(
					"Etes-vous sûr de voulir supprimer le dossier des données ?")) {
				managerGui.execTask( () -> {
					server.dataDelete();
					configureButtons();
				} );
			} else {
				return;
			}
		}
	} 
	
	public void doBackup() {
		managerGui.execTask( () -> {
			server.dataBackup();
		} );
	}
	
	public void doRestore() {
		modelServer.refreshListArchives( listViewServers.getSelectionModel().getSelectedItem() );
		managerGui.showView( ViewServerRestore.class );
	}
	
	public void doExit() {
		managerGui.exit();;
	}
	
	
	//-------
	// Gestionnaires d'evennement
	//-------
	
	public void handleClickOnListView( MouseEvent event) {
		if ( event.getButton() == PRIMARY
				&& event.getClickCount() == 2 ) {
			if ( statusData == ABSENT
					&& ! buttonCreateDelete.isDisabled() ) {
				doCreateDelete();
			}
			if ( statusData == PRESENT 
					&& ! buttonStartStop.isDisabled() ) {
				flagControl = event.isControlDown();
				doStartStop();
				managerGui.execTask( () -> {
					flagControl = false;
				} );
			}
		}
	}
	
	public void handleClickOnButtontStart( MouseEvent event ) {
		if ( event.isAltDown() ) {
			server.kill();
			return;
		}
		flagControl = event.isControlDown();
		doStartStop();
		managerGui.execTask( () -> {
			flagControl = false;
		} );
	}
	
	public void handleClickOnButtontClient1( MouseEvent event ) {
		flagControl = event.isControlDown();
		doClient1();
		managerGui.execTask( () -> {
			flagControl = false;
		} );
	}
	
	public void handleClickOnButtontClient2( MouseEvent event ) {
		flagControl = event.isControlDown();
		doClient2();
		managerGui.execTask( () -> {
			flagControl = false;
		} );
	}
	
	public void handleKeyPresseds( KeyEvent event ) {
		if( event.getCode() == KeyCode.F5 ) {
			modelServer.reinitStatus();
		}
		if( event.getCode() == KeyCode.F6 ) {
			modelServer.refreshListServers();
		}
	}
    
	
	// Classe auxiliaires
	
    static class CellServer extends ListCell<Server> {
        @Override
        public void updateItem(Server item, boolean empty) {
            super.updateItem(item, empty);
            Rectangle rect = new Rectangle(20, 20);
            if (item == null) {
                setGraphic(null);
                setText( null );
            } else {
            	rect.setFill( item.getStatusServer().getColor() );
                setGraphic(rect);
                setText( item.getName() );
            }
        }
    }
    
    
    // Méthodes auxiliair(es
    
    private void configureButtons() {
	
    	buttonStartStop.setDisable(true);
    	buttonCreateDelete.setDisable(true);
    	buttonClient1.setDisable(true);
    	buttonClient2.setDisable(true);
    	buttonInfo.setDisable(true);
    	buttonBackup.setDisable(true);
    	buttonRestore.setDisable(true);
    	
    	if ( server == null ) {
    		return;
    	} 
    	buttonInfo.setDisable(false);
    	statusServer = server.getStatusServer();
    	statusData = server.getStatusData();
    	
    	if ( statusServer == UNAVAILABLE  && ! server.isFlagNoData() )  {
    		buttonCreateDelete.setDisable( false );
        	buttonRestore.setDisable(false);
    		buttonCreateDelete.setText(  "Create" );
    	}

    	if ( statusServer == STOPPED )  {

    		buttonStartStop.setText( "Start" );
        	buttonStartStop.setDisable(false);
    		buttonCreateDelete.setText(  "Delete" );
        	buttonBackup.setDisable(false);

        	if ( statusData != UNKNOWN ) {
        		buttonCreateDelete.setDisable( false );
            	buttonRestore.setDisable(false);
        	}
        	
    	} else if ( statusServer == STARTED )  {
        	buttonStartStop.setDisable(false);
        	buttonStartStop.setText( "Stop" );
        	buttonClient1.setDisable( ! server.isFlagClient1() );
        	buttonClient2.setDisable( ! server.isFlagClient2() );
        	tooltipClient1.setText( server.getTextToolTipClient1() );
        	tooltipClient2.setText( server.getTextToolTipClient2() );
    	}
    	
    }
}
