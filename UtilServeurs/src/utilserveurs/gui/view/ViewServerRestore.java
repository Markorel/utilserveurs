package utilserveurs.gui.view;

import static javafx.scene.input.MouseButton.PRIMARY;

import java.nio.file.Path;

import jakarta.inject.Inject;
import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import jfox.javafx.view.ControllerAbstract;
import jfox.javafx.view.IManagerGui;
import utilserveurs.model.ModelServer;

public class ViewServerRestore extends ControllerAbstract  {

	
	// Composants visuels
	
	@FXML
	private ListView<Path>	listViewArchives;
	@FXML
	private Button			buttonRestore;
	@FXML
	private Button			buttonDelete;
	
	
	// Autres champs

	@Inject
	private IManagerGui		managerGui;
	@Inject
	private ModelServer		modelServer;
	
	
	// Initialisation 

	@FXML
	public void initialize() {

		listViewArchives.setItems( modelServer.getObsListArchives() );
		
		listViewArchives.setCellFactory( listView -> new ListCell<Path>() {
	        @Override
	        public void updateItem(Path item, boolean empty) {
	            super.updateItem(item, empty);
	            if (item != null) {
	                setText( item.getFileName().toString() );
	            } else {
	            	setText( null );
	            }
	        }
		});

		listViewArchives.getSelectionModel().getSelectedItems().addListener(
				 (ListChangeListener<Path>) ( c ) -> {
					 configureButtons();
				 });
		
    	configureButtons();
	}

	
	
	// Actons
	
	public void doRestore() {
		Path path = listViewArchives.getSelectionModel().getSelectedItem();
		boolean ok = managerGui.showDialogConfirm(
				"Etes-vous sûr de voulir restaurer cette sauvegarde ?\n" + path.getFileName().toString() ); 
		if ( ok ) {
			managerGui.execTask( () -> {
				modelServer.restoreArchive( path );
				Platform.runLater( () -> {
//					managerGui.showDialogMessage("Restauration effectuée.");
					try {
						managerGui.showView( ViewServerList.class );
					} catch (Exception e) {
						managerGui.showDialogError(e);
					}
				} );
			} );
		}
	}
	
	public void doDelete() {
		Path path = listViewArchives.getSelectionModel().getSelectedItem();
		if (managerGui.showDialogConfirm(
				"Etes-vous sûr de voulir supprimer cette sauvegarde ?\n" + path.getFileName().toString() )) {
			modelServer.deleteArchive( path );
		}
	}
	
	public void doBack() {
		managerGui.showView( ViewServerList.class );
	}
	
	
	// Gestionnaires d'evennement
	
	public void handleClickOnListView( MouseEvent event) {
		if ( event.getButton() == PRIMARY
				&& event.getClickCount() == 2 ) {
			doRestore();
		}
	}
	
    
    // Méthodes auxiliair(es
    
    private void configureButtons() {
    	
    	buttonDelete.setDisable(true);
    	buttonRestore.setDisable(true);
    	if ( listViewArchives.getSelectionModel().getSelectedIndex() != -1 ) {
        	buttonDelete.setDisable(false);
        	buttonRestore.setDisable(false);
    	}
    	
    }
    

}
