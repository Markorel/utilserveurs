package utilserveurs.gui;

import javafx.scene.Scene;
import javafx.scene.image.Image;
import jfox.javafx.view.ManagerGuiAbstract;
import jfox.javafx.view.View;
import utilserveurs.gui.view.ViewServerList;


public class ManagerGui extends ManagerGuiAbstract {
	
	
	// Actions

	@Override
	public void configureStage()  {
		
		// Choisit la vue à afficher
		showView( ViewServerList.class );
		
		// Configure le stage
		stage.sizeToScene();
		stage.setResizable( false );
		stage.getIcons().add(new Image(getClass().getResource("icone.png").toExternalForm()));

	}


	@SuppressWarnings("exports")
	@Override
	public Scene createScene( View view ) {
		Scene scene = new Scene( view.getRoot() );
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		return scene;
	}
	
}