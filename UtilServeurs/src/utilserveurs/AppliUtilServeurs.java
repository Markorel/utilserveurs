package utilserveurs;

import static utilserveurs.util.Util.KEY_PATH_DIR_CURRENT;
import static utilserveurs.util.Util.PROPS_GLOBAL;
import static utilserveurs.util.Util.PROPS_LOGIING;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import jfox.context.ContextGlobal;
import jfox.context.ContextLocal;
import jfox.context.IContext;
import utilserveurs.gui.ManagerGui;
import utilserveurs.util.Util;
import utilserveurs.util.XProperties;


public class AppliUtilServeurs extends Application {

	//-------
	// Constantes publiques
	//-------

	public static final String	APPLI_NOM = "UtilServeurs";
	public static final String	APPLI_VERSION = "2024-07";
	public static final String	AUTEUR = "© 2024 AMBLARD Emmanuel";
	public static final String	ORGANISME = "";
	
	//-------
	// Logger
	//-------
	
	private static final Logger logger = getLogger();
	
	//-------
	// Champs
	//-------
	
	private IContext	context;
	
	
	//-------
	// Actions
	//-------
	
	@Override
	public void init() throws Exception {
		context = new ContextGlobal(); 
	}
	
	@Override
	public final void start(Stage stage) {
		
		try {

			// Initialise propsGlobal
			//-------
			XProperties propsGlobal = new XProperties();
			propsGlobal.setProperty( KEY_PATH_DIR_CURRENT, Path.of("").toAbsolutePath().toString() );
			
			// Charge le fichier de configuration global
			//-------
			propsGlobal.putAll( Util.getProperties( PROPS_GLOBAL ));

			// Récupère les propriétés passées dans la ligne de commande
			//-------
			parseArgs( propsGlobal );
			
			// Normalise les chemins
			Util.normalizePaths(propsGlobal);
			
			// Context
			//-------
			IContext context = new ContextLocal();
			context.addBean( propsGlobal );

			// ManagerGui
			//-------
	    	var managerGui = context.getBean( ManagerGui.class );
	    	managerGui.setFactoryController( context::getBean );
			managerGui.setStage( stage );
			managerGui.configureStage();
			
			// Affiche le stage
			//-------
			stage.setTitle( APPLI_NOM );
			stage.show();
			
		} catch(Exception e) {
			logger.log( Level.SEVERE, e.getMessage(), e );
	        Alert alert = new Alert(AlertType.ERROR);
	        alert.setHeaderText( "Impossible de démarrer l'application." );
	        alert.showAndWait();
	        Platform.exit();
		}
	}
	
	@Override
	public final void stop() throws Exception {

		if (context != null ) {
			context.close();
		}
		
		// Message de fermeture
		logger.config( "\n    Fermeture de l'application" );
	}
	

	
	//-------
	// Méthodes auxiliaires
	//-------
	
    
	private void parseArgs( XProperties propsGlobal ) {
		
		String key;
		String value;
		
		for( String arg : this.getParameters().getUnnamed() ) {
			
			if( arg.startsWith("-D" ) ) {
				int pos = arg.indexOf('=');
				if( pos >= 0 ) {
					key = arg.substring(2, pos );
					value = arg.substring( pos+1, arg.length() );
					if ( key.startsWith( "path." ) ) {
						value = Paths.get( value ).normalize().toString();
					}
					propsGlobal.setProperty( key, value );
				}
			}
		}
		
	}

    private static Logger getLogger() {
    	
    	try {

    		// Charge le fichier logging.properties
        	final var propsLogging = Util.getProperties( PROPS_LOGIING );
    		
    		// Si les traces vont être écrite dans un fichier
    		final var valueHandlers = propsLogging.getProperty( "handlers" );
    		if ( valueHandlers.contains( "java.util.logging.FileHandler" ) ) {
        		// Transforme en chemin absolu le contenu de la propriété pattern
        		final var keyPattern = "java.util.logging.FileHandler.pattern";
        		var valuePattern = propsLogging.getProperty( keyPattern );
        		if ( valuePattern != null ) {
        			Path pathPattern = Paths.get( valuePattern ).toAbsolutePath().normalize();
        			propsLogging.setProperty( keyPattern, pathPattern.toString() );
        			// Créer le  dossier si nécessaire
        	        Files.createDirectories( pathPattern.getParent() );
        		}
    			
    		}
    		
    		
    		// Transforme la collection Properties en stream
    		// et l'utilise pour configurer le LogManager
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			propsLogging.store(output, null);
			ByteArrayInputStream input = new ByteArrayInputStream(output.toByteArray());
        	LogManager logManager = LogManager.getLogManager();
			logManager.readConfiguration( input );
			input.close();
			output.close();
			
		} catch ( Exception e) {
			e.printStackTrace();
		}
		
		String nomClasse = Thread.currentThread().getStackTrace()[1].getClassName();
		return Logger.getLogger( nomClasse );
	}

    
	public static void main(String[] args) {
		
//        var fos = new FileOutputStream( FileDescriptor.out );
//        var printStream = new PrintStream( fos, true, StandardCharsets.UTF_8 );
//        System.setOut( printStream );
		
		launch( args);
		System.exit(0);
	}

}
