goto %1


:initialize

CHCP 1252 >NUL
PROMPT $G
CD /D "%~dp0"

::CALL :set-fullpath-root FULLPATH_ROOT
SET FULLPATH^_ROOT=C:\dev24

SET     PATH^_DIR_JDK=jdk-20
SET PATH^_GROUP_UTILS=utils

SET   JAVA^_HOME=%FULLPATH_ROOT%\%PATH_DIR_JDK%
SET    ANT^_HOME=%FULLPATH_ROOT%\%PATH_GROUP_UTILS%\ant
::SET  MAVEN^_HOME=%FULLPATH_ROOT%\%PATH_GROUP_UTILS%\maven
::SET GRADLE^_HOME=%FULLPATH_ROOT%\%PATH_GROUP_UTILS%\gradle
::SET     IJ^_HOME=%FULLPATH_ROOT%\%PATH_GROUP_UTILS%\ij

SET NAME_FILE_RUN=%~n2
SET _find=%NAME_FILE_RUN:*#=%
CALL SET NAME_FILE_RUN=%%NAME_FILE_RUN:#%_find%=%%
SET FULLPATH_FILE_BUILD=%~dp2\%NAME_FILE_RUN:run=build%.xml

SET ANT_OPTS=-Dpolyglot.engine.WarnInterpreterOnly=false -Djava.security.manager=allow

GOTO :EOF


:run-ant-interactive

CALL "%ANT_HOME%\bin\ant.bat" -f "%FULLPATH_FILE_BUILD%" -p
ECHO.
SET /P ANT_TARGETS=Entrez la cible � ex�cuter : 
IF NOT "%ANT_TARGETS%"=="" (
  ECHO. & ECHO.
  CALL "%ANT_HOME%\bin\ant.bat" -f "%FULLPATH_FILE_BUILD%" %ANT_TARGETS%
  ECHO. & PAUSE
)

GOTO :EOF


:run-ant-targets

SET ANT_TARGETS=%*
SET ANT_TARGETS=%ANT_TARGETS:run-ant-targets=%
SET ANT_TARGETS=%ANT_TARGETS::=%
ECHO. %ANT_TARGETS%

CALL "%ANT_HOME%\bin\ant.bat" -f "%FULLPATH_FILE_BUILD%" %ANT_TARGETS%
ECHO. & PAUSE

GOTO :EOF


:set-fullpath-root
SET _CURRENT_PATH=%~dp0 
CD /D "%~dp0\.."
SET _FULLPATH=%CD%
IF "%_FULLPATH:~-1,1%"=="\" (
  IF NOT "%_FULLPATH:~-2,1%"==":" SET _FULLPATH=%_FULLPATH:~-0,-1%
)
SET %~1=%_FULLPATH%
CD /D "%_CURRENT_PATH%"

GOTO :EOF
