@ECHO OFF

SET JAVA_HOME=C:\dev23\jdk-18
SET WILDFLY_HOME=C:\dev23\wildfly-26
SET WILDFLY_BASEDIR=C:\TEMP\serveurs\wildfly-26
SET WILDFLY_URL=http://localhost:9990

SET NOPAUSE=ON

ECHO.
ECHO Démarre le serveur
START "WildFly" "%WILDFLY_HOME%\bin\standalone.bat" -Djboss.server.base.dir=%WILDFLY_BASEDIR%

ECHO.
ECHO Crée l'utilisateur admin
CALL "%WILDFLY_HOME%\bin\add-user.bat"  -u admin -p adminadmin -r ManagementRealm -sc "%WILDFLY_BASEDIR%\configuration"

ECHO. & PAUSE

ECHO.
ECHO Deployment Scanner 
CALL "%WILDFLY_HOME%\bin\jboss-cli.bat" --connect --controller=remote+%WILDFLY_URL% --command=^
/subsystem=deployment-scanner/scanner=default:write-attribute(name="scan-interval",value=1000)
CALL "%WILDFLY_HOME%\bin\jboss-cli.bat" --connect --controller=remote+%WILDFLY_URL% --command=^
/subsystem=deployment-scanner/scanner=default:write-attribute(name="auto-deploy-zipped",value=true)
CALL "%WILDFLY_HOME%\bin\jboss-cli.bat" --connect --controller=remote+%WILDFLY_URL% --command=^
/subsystem=deployment-scanner/scanner=default:write-attribute(name="auto-deploy-exploded",value=true)

ECHO.
ECHO Persistent Sessions 
CALL "%WILDFLY_HOME%\bin\jboss-cli.bat" --connect --controller=remote+%WILDFLY_URL% --command=^
/subsystem=undertow/servlet-container=default/setting=persistent-sessions:add(path="session",relative-to="jboss.server.temp.dir")

ECHO.
ECHO Port http
CALL "%WILDFLY_HOME%\bin\jboss-cli.bat" --connect --controller=remote+%WILDFLY_URL% --command=^
/socket-binding-group=standard-sockets/socket-binding=http:write-attribute(^
name=port,value="${jboss.http.port:8930}"^
)

ECHO.
ECHO Port management-http
CALL "%WILDFLY_HOME%\bin\jboss-cli.bat" --connect --controller=remote+%WILDFLY_URL% --command=^
/socket-binding-group=standard-sockets/socket-binding=management-http:write-attribute(^
name=port,value="${jboss.http.port:8931}"^
)


ECHO.
REM Arrête le serveur
CALL "%WILDFLY_HOME%\bin\jboss-cli.bat" --controller=remote+%WILDFLY_URL% --connect --command=:shutdown
ping localhost -n 4 > NUL 

ECHO.
ECHO Supprime les fichiers inutiles
DEL "%WILDFLY_BASEDIR%\configuration\standalone-*.xml"
RMDIR /S /Q "%WILDFLY_BASEDIR%\configuration\standalone_xml_history"
RMDIR /S /Q "%WILDFLY_BASEDIR%\data"
RMDIR /S /Q "%WILDFLY_BASEDIR%\lib"
RMDIR /S /Q "%WILDFLY_BASEDIR%\log"
RMDIR /S /Q "%WILDFLY_BASEDIR%\tmp"

ECHO.
ECHO Ouvrel'explorateur de fichiers
START explorer.exe "%WILDFLY_BASEDIR%"

ECHO. & PAUSE
