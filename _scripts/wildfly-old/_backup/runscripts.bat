@ECHO OFF
SETLOCAL
CHCP 65001 >NUL
CD /D "%~dp0"
PROMPT $G

SET JAVA_HOME=C:\dev23\jdk-18
SET WILDFLY_HOME=C:\dev23\wildfly-26
SET WILDFLY_URL=http://localhost:9990
SET NOPAUSE=ON

:MENU
ECHO.
ECHO Actions :
ECHO.
ECHO     1. Configuration pour UtilServeurs
ECHO.
ECHO     0. Quitter
ECHO.
SET CHOIX=
SET /P CHOIX=Votre choix : 

FOR %%p IN (%CHOIX%) DO CALL :executer_action %%p
IF "%CHOIX%" == "" GOTO FIN
IF "%CHOIX%" == "0" GOTO FIN
GOTO MENU

:FIN
ENDLOCAL
rem PAUSE
GOTO :EOF


:executer_action
SET SCRIPT=
IF /I [%1]==[1]  SET SCRIPT=config-for-utilserveurs.cli
IF NOT [%SCRIPT%]==[] (
rem   CALL "%WILDFLY_HOME%\bin\jboss-cli.bat" --connect --controller=remote+%WILDFLY_URL% --file="%~dp0%SCRIPT%"
  CALL "%WILDFLY_HOME%\bin\jboss-cli.bat" --connect --controller=remote+%WILDFLY_URL% --file="%SCRIPT%"
  ECHO,
)
GOTO :EOF

