@ECHO OFF
CHCP 65001 >NUL
PROMPT $G
CD /D "%~dp0"

:: Avant d'exécuter ce script, il faut :
::   - Copier le dossier standalone d'origine 
::     dansle dossier data C:\TEMP\serveurs-devnn
::   - Exécuter ce script et attendre que WildFly ait démérré
::   - Appuyer sur une touche pour continuer l'exécution du script

SET VERSION_WILDFLY=32

SET FULLPATH_ROOT=C:\dev25
SET  PATH_DIR_JDK=jdk-22
SET     JAVA_HOME=%FULLPATH_ROOT%\%PATH_DIR_JDK%
SET PATH_GROUP_SRV=%FULLPATH_ROOT%\UtilServeurs\srv

SET  PORT_HTTP=8930
SET PORT_HTTPS=8931
SET PORT_ADMIN=9990
SET ADMIN_USER=admin
SET  ADMIN_PWD=admin

SET _CONNECT=--connect --controller=localhost:%PORT_ADMIN%

:: Le dossier data a un nom suffixé par devnn
CALL :set-name NAME_ROOT %FULLPATH_ROOT%
SET PATH_GROUP_DATA=C:\TEMP\serveurs-%NAME_ROOT%

::: Parmètres utilisatn la version de Wildfly
SET   SERVER_NAME=WildFly %VERSION_WILDFLY%
SET NAME_DIR_SOFT=wildfly-%VERSION_WILDFLY%
SET NAME_DIR_DATA=wildfly-%VERSION_WILDFLY%

SET PATH_DIR_DATA=%PATH_GROUP_DATA%\%NAME_DIR_DATA%
SET WILFLY_HOME=%PATH_GROUP_SRV%\%NAME_DIR_SOFT%

ECHO.
ECHO Démarre WildFly
SET NOPAUSE=ON
START "%SERVER_NAME%" CMD /C %WILFLY_HOME%\bin\standalone.bat -Djboss.server.base.dir="%PATH_DIR_DATA%"
ECHO. & PAUSE

ECHO.
ECHO Crée l'utilisateur admin
CALL %WILFLY_HOME%\bin\add-user.bat -u %ADMIN_USER% -p %ADMIN_PWD% -r ManagementRealm -sc "%PATH_DIR_DATA%\configuration"


ECHO.
ECHO Change les ports HTTP et HTTPS
SET _CMD=^
cd /socket-binding-group=standard-sockets^
, ./socket-binding=http:write-attribute(name="port", value="${jboss.http.port:%PORT_HTTP%}")^
, ./socket-binding=https:write-attribute(name="port", value="${jboss.https.port:%PORT_HTTPS%}")^
, reload
CALL %WILFLY_HOME%\bin\jboss-cli.bat %_CONNECT% --commands="%_CMD%"


ECHO.
ECHO Configure le deployment scanner
SET _CMD=^
cd /subsystem=deployment-scanner/scanner=default^
, :write-attribute(name=auto-deploy-exploded,value=true)^
, :write-attribute(name=auto-deploy-zipped,value=true)^
, :write-attribute(name=scan-interval,value=1000)^
, reload
CALL %WILFLY_HOME%\bin\jboss-cli.bat %_CONNECT% --commands="%_CMD%"

ECHO.
ECHO Configure ses sessions persistantes
SET _CMD=^
/subsystem=undertow/servlet-container=default/setting=persistent-sessions:add(^
  path=session, relative-to=jboss.server.temp.dir)^
, reload
CALL %WILFLY_HOME%\bin\jboss-cli.bat %_CONNECT% --commands="%_CMD%"

ECHO.
ECHO Crée les headers et les affecte à l'hôte par défaut
SET _CMD=^
cd /subsystem=undertow/configuration=filter^
, ./response-header=header-server:add(header-name=Server,header-value=%SERVER_NAME%)^
, ./response-header=header-utilserveurs-id:add(header-name=Utilserveurs-Id,header-value=${jboss.server.base.dir})^
, cd /subsystem=undertow/server=default-server/host=default-host^
, ./filter-ref=header-server:add^
, ./filter-ref=header-utilserveurs-id:add
CALL %WILFLY_HOME%\bin\jboss-cli.bat %_CONNECT% --commands="%_CMD%"

ECHO.
ECHO Arrête WildFly
SET NOPAUSE=ON
CALL %WILFLY_HOME%\bin\jboss-cli.bat %_CONNECT% --command=:shutdown
ping localhost -n 4 > NUL 


ECHO.
ECHO Supprime les fichiers inutiles
DEL "%PATH_DIR_DATA%\configuration\standalone-*.xml"
RMDIR /S /Q "%PATH_DIR_DATA%\configuration\standalone_xml_history"
RMDIR /S /Q "%PATH_DIR_DATA%\data"
RMDIR /S /Q "%PATH_DIR_DATA%\log"
RMDIR /S /Q "%PATH_DIR_DATA%\tmp"

ECHO.
ECHO Ouvre l'explorateur de fichiers
START explorer.exe "%PATH_DIR_DATA%"


ECHO. & PAUSE
GOTO :EOF


:set-name
SET %~1=%~n2
GOTO :EOF